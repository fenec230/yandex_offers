# YandexOffers

A Ruby client for YandexOffers API

API Doc: https://tech.yandex.ru/money/doc/offers/about-docpage

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'yandex_offers'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install yandex_offers

## Usage

1. Set configuration
```ruby
YandexOffers.configure do |config|
  config.login = "login"
  config.password = "password"
  config.base_url = "base_url"
end
```

2. Use Client
```ruby
client = YandexOffers::Client.new
client.campaigns.start({ymCampaignId: 1})
```

It performs `campaigns/create` request to API

## TODO

1. Add argument validation in api methods
2. Add error handling
2. Improve test coverage

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/fenec/yandex_offers.

require "spec_helper"

RSpec.describe YandexOffers::Client do
  describe "#method_missing" do
    it "creates an instance of Api class" do
      client = YandexOffers::Client.new
      expect(client.campaigns).to be_a(YandexOffers::Api::V1::Campaigns)
    end

    context "a class doesn't exist" do
      it "raises a error" do
        client = YandexOffers::Client.new
        expect{ client.non_existent_stuff }.to raise_error(NameError)
      end
    end
  end
end

require "spec_helper"

RSpec.describe YandexOffers::Authorization do
  before do
    YandexOffers.configure do |config|
      config.login = "login"
      config.password = "password"
      config.base_url = "http://some_url.com"
    end
  end

  describe "#get_token" do
    before do
      stub_request(:post, "http://some_url.com/users/login").
        with(body: "login=login&password=password").
        to_return(body: {"accessToken":"token","tokenType":"Bearer","expiresIn":1}.to_json)
    end

    it "returns token" do
      expect(YandexOffers::Authorization.new.get_token).to eq("token")
    end
  end

end

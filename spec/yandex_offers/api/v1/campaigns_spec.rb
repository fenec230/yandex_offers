require "spec_helper"

RSpec.describe YandexOffers::Api::V1::Campaigns do
  before do
    YandexOffers.configure do |config|
      config.login = "login"
      config.password = "password"
      config.base_url = "http://some_url.com"
    end

    stub_request(:post, "http://some_url.com/users/login").
         with(body: "login=login&password=password").
         to_return(status: 200, body: {"accessToken": "token"}.to_json, headers: {})
  end

  describe "#create" do
    let(:request) { fixture("campaigns/create/request.json") }
    let(:response) { fixture("campaigns/create/response.json") }

    it "responds" do
      stub_request(:post, "http://some_url.com/campaigns/create").
      to_return(body: response)

      expect(YandexOffers::Api::V1::Campaigns.new.create(request)).to eq(JSON.parse(response))
    end
  end

  describe "#start" do
    let(:response) { fixture("campaigns/start/response.json") }

    it "responds" do
      stub_request(:post, "http://some_url.com/campaigns/start").
      to_return(body: response)

      expect(YandexOffers::Api::V1::Campaigns.new.start({"ymCampaignId": 34})).to eq(JSON.parse(response))
    end
  end

  describe "#stop" do
    let(:response) { fixture("campaigns/stop/response.json") }

    it "responds" do
      stub_request(:post, "http://some_url.com/campaigns/stop").
      to_return(body: response)

      expect(YandexOffers::Api::V1::Campaigns.new.stop({"ymCampaignId": 34})).to eq(JSON.parse(response))
    end
  end

  describe "#get" do
    let(:response) { fixture("campaigns/get/response.json") }

    it "responds" do
      stub_request(:post, "http://some_url.com/campaigns/get").
      to_return(body: response)

      expect(YandexOffers::Api::V1::Campaigns.new.get({"ymCampaignId": 34})).to eq(JSON.parse(response))
    end
  end

  describe "#get_all_campaigns" do
    let(:response) { fixture("campaigns/get_all_campaigns/response.json") }

    it "responds" do
      stub_request(:post, "http://some_url.com/campaigns/get-all-campaigns").
      to_return(body: response)

      expect(YandexOffers::Api::V1::Campaigns.new.get_all_campaigns({"limit": 3, "offset": 0})).to eq(JSON.parse(response))
    end
  end

  describe "#add_pins" do
    let(:request) { fixture("campaigns/add_pins/request.json") }
    let(:response) { fixture("campaigns/add_pins/response.json") }

    it "responds" do
      stub_request(:post, "http://some_url.com/campaigns/add-pins").
      to_return(body: response)

      expect(YandexOffers::Api::V1::Campaigns.new.add_pins(request)).to eq(JSON.parse(response))
    end
  end
end

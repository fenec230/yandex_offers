require "spec_helper"

RSpec.describe YandexOffers do
  before do
    YandexOffers.configure do |config|
      config.login = "login"
      config.password = "password"
      config.base_url = "base_url"
    end
  end

  it "sets configuration data" do
    expect(YandexOffers.configuration.login).to eq("login")
    expect(YandexOffers.configuration.password).to eq("password")
    expect(YandexOffers.configuration.base_url).to eq("base_url")
  end

end

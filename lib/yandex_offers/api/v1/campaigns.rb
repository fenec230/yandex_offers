module YandexOffers
  module Api
    module V1
      class Campaigns
        def create(args)
          Request.post_with_authorization("/campaigns/create", args)
        end

        def start(args)
          Request.post_with_authorization("/campaigns/start", args)
        end

        def stop(args)
          Request.post_with_authorization("/campaigns/stop", args)
        end

        def get(args)
          Request.post_with_authorization("/campaigns/get", args)
        end

        def get_all_campaigns(args)
          Request.post_with_authorization("/campaigns/get-all-campaigns", args)
        end

        def add_pins(args)
          Request.post_with_authorization("/campaigns/add-pins", args)
        end
      end
    end
  end
end

module YandexOffers
  class Authorization
    def get_token
      body = { login: YandexOffers.configuration.login, password: YandexOffers.configuration.password }
      response = Request.post("/users/login", body)
      response["accessToken"]
    end
  end
end

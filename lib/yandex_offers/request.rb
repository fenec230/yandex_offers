require 'httparty'

class YandexOffers::Request
  include HTTParty

  class << self
    def post(url, body)
      options = {}
      options[:format] = :json
      options[:body] = body
      HTTParty.post("#{YandexOffers.configuration.base_url}#{url}", options).parsed_response
    end

    def post_with_authorization(url, body)
      options = {}
      options[:format] = :json
      options[:body] = body.to_json
      options[:headers] = self.headers.merge({"Authorization" => "Bearer #{self.token}"})
      HTTParty.post("#{YandexOffers.configuration.base_url}#{url}", options).parsed_response
    end

    def token
      @token ||= YandexOffers::Authorization.new.get_token
    end

    def headers
      @headers ||= { "Content-Type" => "application/json", "Accept" => "application/json" }
    end
  end
end

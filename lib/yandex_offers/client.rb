module YandexOffers
  class Client
    def method_missing(name, *args, &block)
      klass_name = name.to_s.capitalize
      YandexOffers::Api::V1.const_get("#{klass_name}").new
    end
  end
end

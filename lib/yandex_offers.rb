require "yandex_offers/version"
require "yandex_offers/authorization"
require "yandex_offers/configuration"
require "yandex_offers/client"
require "yandex_offers/request"
require "yandex_offers/api/v1/campaigns"

module YandexOffers
  class << self
    def configure
      yield configuration
    end

    def configuration
      @configuration ||= Configuration.new
    end
  end
end
